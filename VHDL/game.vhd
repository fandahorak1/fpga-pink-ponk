Library IEEE;
use IEEE.STD_Logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
entity vgatest is
 port(clock, left_up, left_down, right_up, right_down, start : in std_logic;
       R, G, B, H, V : out std_logic);
end entity;

architecture test of vgatest is

 component vgadrive is
  port( clock          : in std_logic;  -- 25.175 Mhz clock
   red, green, blue : in std_logic;  -- input values for RGB signals
   row, column      : out std_logic_vector(9 downto 0); -- for current pixel
   Rout, Gout, Bout, H, V : out std_logic); -- VGA drive signals
 end component;
 -- Procedure zur zufälligen Ermittlung der Ballbewegung (in rand: Zufallszahl, out x,y: Vektor)
 procedure rand_vec(rand : in integer range 0 to 28 := 0; x,y : out integer range -3 to 3) is
 begin
  case (rand) is 
   when 0 =>
    x := -1;
    y := -1;
   when 1 =>
    x := 1;
    y := -1;
   when 2 =>
    x := 1;
    y := 1;
   when 3 =>
    x := -1;
    y := 1;
   when 4 =>
    x := -2;
    y := -1;
   when 5 =>
    x := -1;
    y := -2;
   when 6 =>
    x := 1;
    y := -2;
   when 7 =>
    x := 2;
    y := -1;
   when 8 =>
    x := 2;
    y := 1;
   when 9 =>
    x := 1;
    y := 2;
   when 10 =>
    x := -1;
    y := 2;
   when 11 =>
    x := -2;
    y := 1;
   when 12 =>
    x := -3;
    y := -1;
   when 13 =>
    x := -1;
    y := -3;
   when 14 =>
    x := 1;
    y := -3;
   when 15 =>
    x := 3;
    y := -1;
   when 16 =>
    x := 3;
    y := 1;
   when 17 =>
    x := 1;
    y := 3;
   when 18 =>
    x := -1;
    y := 3;
   when 19 =>
    x := -3;
    y := 1;
   when 20 =>
    x := -3;
    y := -2;
   when 21 =>
    x := -2;
    y := -3;
   when 22 =>
    x := 2;
    y := -3;
   when 23 =>
    x := 3;
    y := -2;
   when 24 =>
    x := 3;
    y := 2;
   when 25 =>
    x := 2;
    y := 3;
   when 26 =>
    x := -2;
    y := 3;
   when 27 =>
    x := -3;
    y := 2;
   when others =>
    x := -1;
    y := -1;
  end case;   
 end procedure rand_vec;
 
 signal row, column : std_logic_vector(9 downto 0); -- X,Y-Position aktuellen Bildpixels
 signal red, green, blue : std_logic := '0'; -- Farbsteuerung
 -- Balkenposition (Y) für Spieler links und rechts
 signal beam_pos_left, beam_pos_right: integer range 0 to 650 := 0;
 -- Ballkoordinaten
 signal ball_pos_x, ball_pos_y: integer range 0 to 650 := 0;
 -- Spielstand für links und rechts
 signal s_l, s_r : integer range 0 to 10 := 0;
begin
 VGA : component vgadrive -- Einbindung des VGA Drivers
  port map ( clock => clock, red => red, green => green, blue => blue,
               row => row, column => column,
               Rout => R, Gout => G, Bout => B, H => H, V => V);
 
 control: process(clock, start) -- Spielphysik und Steuerung
  variable random : integer range 0 to 28 := 0; -- Zufallszahl
  variable dir_x, dir_y : integer range -3 to 3; -- Bewegungsvektor des Balls
  -- Zählvariablen für Zeitteilung vom Clocksignal
  variable cnt_ball, cnt_balken, cnt_ball_mod : integer range 0 to 101 := 0;
  variable cnt : integer range 0 to 2002 := 0;
 begin
  if (clock'event and clock = '1') then
   if (cnt /= 2000) then -- Runterskalierung des Clocksignals
    cnt := cnt +1;
   else
    cnt := 0;
    cnt_ball := cnt_ball + 1; -- weiter Skalierung des Takts
    cnt_balken := cnt_balken + 1; -- weiter Skalierung des Takts
    random  := random  + 1; -- Zufallszahl wird durchgezählt
    if (random = 28) then -- Zurücksetzten der Zufallszahl (0..27)
     random := 0;
    end if;
    if (cnt_ball = 101) then --Zurücksetzten des Zählers
     cnt_ball := 0;
    end if;
    if (cnt_balken = 76) then --Zurücksetzten des Zählers
     cnt_balken := 0;
    end if;
    -- Resetverhalten
    if (start = '0') then 
     ball_pos_x <= 320; -- Ball wird in die Mitte positioniert
     ball_pos_y <= 240;
     s_l <= 0; -- Sore zurücksetzen
     s_r <= 0;
     cnt_ball_mod := 100; -- Ballgeschwindigkeit auf langsam
     -- Auruf der Zufallsfunktion um zufälligen Bewegungsverktor zu erhalten
     rand_vec(rand => random, x => dir_x, y => dir_y);
    else
     -- Spielstart
     -- Ballbewegung
     if (cnt_ball >= cnt_ball_mod) then
      ball_pos_y <= ball_pos_y + dir_y; -- Ballbewegung Y
      ball_pos_x <= ball_pos_x + dir_x; -- Ballbewegung X
      cnt_ball := 0; -- Counter zurücksetzten
      -- oberer Rand detektiert
      if (ball_pos_y <= 3) then -- Abfragung vorher, weil Bewegung >1 sein kann
       dir_y := (-1*dir_y); -- "Abprallen" - Y Vektor negiert
       ball_pos_y <= 4;
      end if;
      -- unterer Rand detektiert
      if (ball_pos_y >= (479-15)) then -- Abfragung vorher, weil Bewegung >1 sein kann
       dir_y := (-1*dir_y); -- "Abprallen" - Y Vektor negiert
       ball_pos_y <= 479-16;
      end if;
      -- Ball trift auf Balken (rechts)
      if (ball_pos_x >= (623-15) and dir_x > 0 and ball_pos_y > (beam_pos_right - 14) and ball_pos_y < (beam_pos_right + 70 + 14)) then
       dir_x := (-1*dir_x); -- "Abprallen" - X Vektor negiert
       if (cnt_ball_mod >= 5) then -- Erhöhung der Ballgeschwindigkeit
        cnt_ball_mod := cnt_ball_mod - 3;
       end if;
      end if;
      -- Ball trift auf Balken (links)
      if (ball_pos_x <= 16 and dir_x < 0 and ball_pos_y > (beam_pos_left - 14) and ball_pos_y < (beam_pos_left + 70 + 14)) then
       dir_x := (-1*dir_x); -- "Abprallen" - X Vektor negiert
       if (cnt_ball_mod >= 5) then -- Erhöhung der Ballgeschwindigkeit
        cnt_ball_mod := cnt_ball_mod - 3;
       end if;
      end if;
      -- Ball trift auch rechten Rand (linker Spieler punktet)
      if (ball_pos_x >= (639-15-10)) then
       ball_pos_x <= 320; -- Ball in die Mitte
       ball_pos_y <= 240; -- Ball in die Mitte
       s_l <= s_l + 1; -- Punkte erhöhen
       cnt_ball_mod := 100; -- Ballgeschwindigkeit wieder auf langsam
       -- Zufallsvektor für Ballbewegung bestimmen
       rand_vec(rand => random, x => dir_x, y => dir_y);
      end if; 
      -- Ball trift auch linkten Rand (rechter Spieler punktet)
      if (ball_pos_x <= 10) then
       ball_pos_x <= 320; -- Ball in die Mitte
       ball_pos_y <= 240; -- Ball in die Mitte
       s_r <= s_r + 1; -- Punkte erhöhen
       cnt_ball_mod := 100; -- Ballgeschwindigkeit wieder auf langsam
       -- Zufallsvektor für Ballbewegung bestimmen
       rand_vec(rand => random, x => dir_x, y => dir_y);
      end if;
     end if;
     -- Score wird zurückgesetzt wenn ein Spieler gewinnt
     if (s_l = 10) or (s_r = 10) then
      s_l <= 0;
      s_r <= 0;
     end if;
     -- Bewegung der Balken bei Tastendruck, Geschwindigkeit durch cnt_balken
     -- einstellbar. Randpositionen werden berücksichtigt.
     if (cnt_balken >= 75) then
      if (left_up = '1' and left_down = '0' and beam_pos_left > 1) then
       beam_pos_left <= beam_pos_left - 2; -- linker Balken - aufwärts
      end if;
      if (left_up = '0' and left_down = '1' and beam_pos_left < 408) then
       beam_pos_left <= beam_pos_left + 2; -- linker Balken - abwärts
      end if;
      if (right_up = '1' and right_down = '0' and beam_pos_right > 1) then
       beam_pos_right <= beam_pos_right - 2; -- rehter Balken - aufwärts
      end if;
      if (right_up = '0' and right_down = '1' and beam_pos_right < 408) then
       beam_pos_right <= beam_pos_right + 2; -- rechter Balken - abwärts
      end if;
     end if;
    end if;
   end if;
   end if;
 end process;
 
 RGB : process(row, column) -- VGA Ausgabe
  variable rgb : std_logic_vector (2 downto 0) := "000"; -- rot, grün, blau für VGADriver
 begin
  -- linker Balken (15x70 Pixel)
  if (row > beam_pos_left and row < (beam_pos_left + 70) and column > 0 and column < 15)  then
  rgb := "100"; -- Farbe: rot 1, grün 0, blau, 0
  else
  -- rechter Balken (15x70 Pixel)
   if (row > beam_pos_right and row < (beam_pos_right + 70) and column > 624 and column < 639) then
   rgb := "001";
   else
  -- Mittellinie (6 Pixel breit)
    if (row > 0 and row < 479 and column > 317 and column < 323)  then
    rgb := "111";
    else
  -- linker Punktestand - Seg A --
     if ((row > 20 and row < 45 and column > 275 and column < 280) and (s_l = 0 or s_l = 1 or s_l = 2 or s_l = 3 or s_l = 4 or s_l = 7 or s_l = 8 or s_l = 9))  then
     rgb := "100";
     else
  -- linker Punktestand - Seg B --
     if ((row > 50 and row < 75 and column > 275 and column < 280) and (s_l = 0 or s_l = 1 or s_l = 5 or s_l = 6 or s_l = 3 or s_l = 4 or s_l = 7 or s_l = 8 or s_l = 9)) then
     rgb := "100";
     else
  -- linker Punktestand - Seg C --
     if ((row > 75 and row < 80 and column > 250 and column < 275) and (s_l = 0 or s_l = 2 or s_l = 3 or s_l = 5 or s_l = 6 or s_l = 8 or s_l = 9))  then
     rgb := "100";
     else
  -- linker Punktestand - Seg D --
     if ((row > 50 and row < 75 and column > 245 and column < 250) and (s_l = 0 or s_l = 2 or s_l = 6 or s_l = 8))  then
     rgb := "100";
     else
  -- linker Punktestand - Seg E --
     if ((row > 20 and row < 45 and column > 245 and column < 250) and (s_l = 0 or s_l = 4 or s_l = 5 or s_l = 6 or s_l = 8 or s_l = 9))  then
     rgb := "100";
     else
  -- linker Punktestand - Seg F --
     if ((row > 15 and row < 20 and column > 250 and column < 275) and (s_l = 0 or s_l = 2 or s_l = 3 or s_l = 5 or s_l = 6 or s_l = 7 or s_l = 8 or s_l = 9))  then
     rgb := "100";
     else
  -- linker Punktestand - Seg G --
     if ((row > 45 and row < 50 and column > 250 and column < 275) and (s_l = 2 or s_l = 3 or s_l = 4 or s_l = 5 or s_l = 6 or s_l = 8 or s_l = 9))  then
     rgb := "100";
     else
   -- rechter Punktestand - Seg A --
      if ((row > 20 and row < 45 and column > 390 and column < 395) and (s_r = 0 or s_r = 1 or s_r = 2 or s_r = 3 or s_r = 4 or s_r = 7 or s_r = 8 or s_r = 9))  then
      rgb := "001";
      else
   -- rechter Punktestand - Seg B --
      if ((row > 50 and row < 75 and column > 390 and column < 395) and (s_r = 0 or s_r = 1 or s_r = 5 or s_r = 6 or s_r = 3 or s_r = 4 or s_r = 7 or s_r = 8 or s_r = 9))  then
      rgb := "001";
      else
   -- rechter Punktestand - Seg C --
      if ((row > 75 and row < 80 and column > 365 and column < 390) and (s_r = 0 or s_r = 2 or s_r = 3 or s_r = 5 or s_r = 6 or s_r = 8 or s_r = 9))  then
      rgb := "001";
      else
   -- rechter Punktestand - Seg D --
      if ((row > 50 and row < 75 and column > 360 and column < 365) and (s_r = 0 or s_r = 2 or s_r = 6 or s_r = 8))  then
      rgb := "001";
      else
   -- rechter Punktestand - Seg E --
      if ((row > 20 and row < 45 and column > 360 and column < 365) and (s_r = 0 or s_r = 4 or s_r = 5 or s_r = 6 or s_r = 8 or s_r = 9))  then
      rgb := "001";
      else
   -- rechter Punktestand - Seg F --
      if ((row > 15 and row < 20 and column > 365 and column < 390) and (s_r = 0 or s_r = 2 or s_r = 3 or s_r = 5 or s_r = 6 or s_r = 7 or s_r = 8 or s_r = 9))  then
      rgb := "001";
      else
   -- rechter Punktestand - Seg G --
      if ((row > 45 and row < 50 and column > 365 and column < 390) and (s_r = 2 or s_r = 3 or s_r = 4 or s_r = 5 or s_r = 6 or s_r = 8 or s_r = 9))  then
      rgb := "001";
      else
      -- Ball (15x15 Pixel)
       if (row > ball_pos_y and row < (ball_pos_y + 15) and column > ball_pos_x and column < (ball_pos_x + 15))  then
       rgb := "111";
       else
       rgb := "000";
       end if; 
      end if; 
      end if;
      end if;
      end if;
      end if;
      end if;
      end if; 
     end if;
     end if;
     end if;
     end if;
     end if;
     end if;
     end if;     
    end if;
   end if;
  end if;
  -- Ausgabe der rgb Werte
  red <= rgb(2);
  green <= rgb(1);
  blue <= rgb(0);
 end process;
end architecture;