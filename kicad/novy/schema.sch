EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L tang_nano:tang_nano U1
U 1 1 5FC983F5
P 3500 3350
F 0 "U1" H 3475 4475 50  0000 C CNN
F 1 "tang_nano" H 3475 4384 50  0000 C CNN
F 2 "Package_DIP:DIP-40_W15.24mm" H 3500 3700 50  0001 C CNN
F 3 "" H 3500 3700 50  0001 C CNN
	1    3500 3350
	1    0    0    -1  
$EndComp
$Comp
L Connector:DB15_Female_HighDensity J1
U 1 1 5FCA3117
P 1400 3600
F 0 "J1" H 1400 4467 50  0000 C CNN
F 1 "DB15_Female_HighDensity" H 1400 4376 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x15_P2.54mm_Vertical" H 450 4000 50  0001 C CNN
F 3 " ~" H 450 4000 50  0001 C CNN
	1    1400 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5FCA4525
P 950 3600
F 0 "R3" V 1157 3600 50  0000 C CNN
F 1 "270Ω" V 1066 3600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 880 3600 50  0001 C CNN
F 3 "~" H 950 3600 50  0001 C CNN
	1    950  3600
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2
U 1 1 5FCA596D
P 950 3400
F 0 "R2" V 1157 3400 50  0000 C CNN
F 1 "270Ω" V 1066 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 880 3400 50  0001 C CNN
F 3 "~" H 950 3400 50  0001 C CNN
	1    950  3400
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R1
U 1 1 5FCA5B96
P 950 3200
F 0 "R1" V 743 3200 50  0000 C CNN
F 1 "270Ω" V 834 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 880 3200 50  0001 C CNN
F 3 "~" H 950 3200 50  0001 C CNN
	1    950  3200
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 5FCA622E
P 1850 3600
F 0 "R4" V 2057 3600 50  0000 C CNN
F 1 "100Ω" V 1966 3600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 1780 3600 50  0001 C CNN
F 3 "~" H 1850 3600 50  0001 C CNN
	1    1850 3600
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R5
U 1 1 5FCA675B
P 1850 3800
F 0 "R5" V 2057 3800 50  0000 C CNN
F 1 "100Ω" V 1966 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 1780 3800 50  0001 C CNN
F 3 "~" H 1850 3800 50  0001 C CNN
	1    1850 3800
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x03_Male J2
U 1 1 5FCA9E48
P 4150 2850
F 0 "J2" H 4258 3131 50  0000 C CNN
F 1 "Conn_01x03_Male" H 4258 3040 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4150 2850 50  0001 C CNN
F 3 "~" H 4150 2850 50  0001 C CNN
	1    4150 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	550  2450 3050 2450
Wire Wire Line
	800  3400 600  3400
Wire Wire Line
	600  3400 600  2550
Wire Wire Line
	600  2550 3050 2550
Wire Wire Line
	500  2650 3050 2650
Wire Wire Line
	2000 3600 2000 2750
Wire Wire Line
	2000 2750 3050 2750
Wire Wire Line
	2000 3800 2050 3800
Wire Wire Line
	2050 3800 2050 2850
Wire Wire Line
	2050 2850 3050 2850
Wire Wire Line
	1100 3900 1100 4000
Wire Wire Line
	650  3500 650  3900
Wire Wire Line
	650  3900 1100 3900
Wire Wire Line
	650  3500 750  3500
Connection ~ 1100 3900
Wire Wire Line
	1100 3100 750  3100
Wire Wire Line
	750  3100 750  3300
Connection ~ 750  3500
Wire Wire Line
	750  3500 1100 3500
Wire Wire Line
	1100 3300 750  3300
Connection ~ 750  3300
Wire Wire Line
	750  3300 750  3500
Wire Wire Line
	4050 4250 3950 4250
Wire Wire Line
	3950 4250 3900 4250
Wire Wire Line
	2950 4500 2950 4250
Wire Wire Line
	2950 4150 3050 4150
Wire Wire Line
	1100 4000 1100 4500
Wire Wire Line
	1100 4500 2950 4500
Connection ~ 1100 4000
Connection ~ 2950 4500
Wire Wire Line
	4500 3000 4600 3000
Wire Wire Line
	4600 3000 4600 2850
Wire Wire Line
	4600 2850 4350 2850
$Comp
L Device:R R6
U 1 1 5FCF0966
P 4400 3300
F 0 "R6" H 4470 3346 50  0000 L CNN
F 1 "600Ω" H 4470 3255 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 4330 3300 50  0001 C CNN
F 3 "~" H 4400 3300 50  0001 C CNN
	1    4400 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3150 4400 3050
NoConn ~ 3900 2750
NoConn ~ 3900 2850
NoConn ~ 3900 2950
NoConn ~ 3900 3050
NoConn ~ 3900 3150
NoConn ~ 3900 3250
NoConn ~ 3900 3350
NoConn ~ 3900 3450
NoConn ~ 3050 2950
NoConn ~ 3050 3050
NoConn ~ 3050 3150
NoConn ~ 3050 3250
NoConn ~ 3050 3350
NoConn ~ 3050 3450
NoConn ~ 3050 3550
NoConn ~ 3050 3650
NoConn ~ 3050 3750
NoConn ~ 3050 3850
NoConn ~ 3050 3950
NoConn ~ 3050 4050
NoConn ~ 3900 4050
NoConn ~ 3900 4350
NoConn ~ 3900 3950
NoConn ~ 3900 3850
NoConn ~ 3900 3750
Connection ~ 4500 4050
Wire Wire Line
	4500 4050 4500 3000
Wire Wire Line
	6550 4350 6950 4350
Connection ~ 6550 4350
Wire Wire Line
	6100 4350 6550 4350
Wire Wire Line
	6550 4050 6950 4050
Connection ~ 6550 4050
Wire Wire Line
	6200 4050 6550 4050
Connection ~ 6100 4350
Connection ~ 5800 3450
Wire Wire Line
	6100 3450 6100 4350
Wire Wire Line
	5800 3450 6100 3450
Wire Wire Line
	4400 3450 5800 3450
Wire Wire Line
	5800 2850 6000 2850
Connection ~ 5800 2850
Wire Wire Line
	5800 3150 5800 2850
Connection ~ 6950 4350
Connection ~ 7550 3550
Wire Wire Line
	7550 4350 7350 4350
Wire Wire Line
	7550 3550 7550 4350
Wire Wire Line
	7550 3550 7800 3550
Connection ~ 7800 3150
Wire Wire Line
	7800 3250 7800 3150
Connection ~ 7600 2750
Wire Wire Line
	7550 2750 7550 3250
Wire Wire Line
	7600 2750 7550 2750
$Comp
L Device:R R7
U 1 1 5FCF096C
P 5800 3300
F 0 "R7" H 5870 3346 50  0000 L CNN
F 1 "600Ω" H 5870 3255 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 5730 3300 50  0001 C CNN
F 3 "~" H 5800 3300 50  0001 C CNN
	1    5800 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 5FCEEED9
P 7800 3400
F 0 "R9" H 7870 3446 50  0000 L CNN
F 1 "6kΩ" H 7870 3355 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 7730 3400 50  0001 C CNN
F 3 "~" H 7800 3400 50  0001 C CNN
	1    7800 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5FCEEB02
P 7550 3400
F 0 "R8" H 7620 3446 50  0000 L CNN
F 1 "6k" H 7620 3355 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 7480 3400 50  0001 C CNN
F 3 "~" H 7550 3400 50  0001 C CNN
	1    7550 3400
	1    0    0    -1  
$EndComp
Connection ~ 6950 4050
Connection ~ 7350 2850
Wire Wire Line
	7350 4050 6950 4050
Wire Wire Line
	7350 2850 7350 4050
Wire Wire Line
	4400 3050 6000 3050
Wire Wire Line
	4700 2850 5800 2850
Wire Wire Line
	4700 2750 4700 2850
Wire Wire Line
	4350 2750 4700 2750
Wire Wire Line
	7350 2750 6950 2750
Wire Wire Line
	7350 2850 7350 2750
Wire Wire Line
	7800 2850 7350 2850
Wire Wire Line
	7600 2950 6950 2950
Wire Wire Line
	7600 2750 7600 2950
Wire Wire Line
	7800 2750 7600 2750
Wire Wire Line
	7800 3150 6950 3150
Wire Wire Line
	7800 2950 7800 3150
Wire Wire Line
	6200 4050 5850 4050
Connection ~ 6200 4050
Wire Wire Line
	6200 4500 2950 4500
Wire Wire Line
	6200 4050 6200 4500
Connection ~ 5850 4050
Wire Wire Line
	5850 4050 4800 4050
Connection ~ 5850 4350
Wire Wire Line
	5850 4350 6100 4350
Wire Wire Line
	4500 4350 4800 4350
Wire Wire Line
	4650 2950 6000 2950
Wire Wire Line
	4650 4200 4650 2950
Wire Wire Line
	7050 3050 6950 3050
Wire Wire Line
	7050 3400 7050 3050
Wire Wire Line
	6700 3400 7050 3400
Wire Wire Line
	6700 4200 6700 3400
Wire Wire Line
	7100 2850 6950 2850
Wire Wire Line
	7100 4200 7100 2850
Wire Wire Line
	6000 3150 6000 4200
Wire Wire Line
	7250 2550 6950 2550
Wire Wire Line
	7250 3550 7250 2550
Wire Wire Line
	3900 3550 7250 3550
Wire Wire Line
	7200 3650 3900 3650
Wire Wire Line
	7200 2650 7200 3650
Wire Wire Line
	6950 2650 7200 2650
Wire Wire Line
	3900 2650 6000 2650
Wire Wire Line
	6000 2550 3900 2550
$Comp
L Connector:Conn_01x03_Male J3
U 1 1 5FCAB8B2
P 8000 2850
F 0 "J3" H 7972 2782 50  0000 R CNN
F 1 "Conn_01x03_Male" H 7972 2873 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 8000 2850 50  0001 C CNN
F 3 "~" H 8000 2850 50  0001 C CNN
	1    8000 2850
	-1   0    0    1   
$EndComp
$Comp
L tang_nano:LM339N U2
U 1 1 5FCA0A53
P 6450 2850
F 0 "U2" H 6475 3465 50  0000 C CNN
F 1 "LM339N" H 6475 3374 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6450 3350 50  0001 C CNN
F 3 "" H 6450 3350 50  0001 C CNN
	1    6450 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT_TRIM RV3
U 1 1 5FC9CEA9
P 6550 4200
F 0 "RV3" H 6481 4246 50  0000 R CNN
F 1 "10kΩ" H 6481 4155 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3386C_Horizontal" H 6550 4200 50  0001 C CNN
F 3 "~" H 6550 4200 50  0001 C CNN
	1    6550 4200
	1    0    0    1   
$EndComp
$Comp
L Device:R_POT_TRIM RV1
U 1 1 5FC9CEA3
P 4500 4200
F 0 "RV1" H 4450 4250 50  0000 R CNN
F 1 "10kΩ" H 4550 4000 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3386C_Horizontal" H 4500 4200 50  0001 C CNN
F 3 "~" H 4500 4200 50  0001 C CNN
	1    4500 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT_TRIM RV4
U 1 1 5FC9C907
P 6950 4200
F 0 "RV4" H 6881 4246 50  0000 R CNN
F 1 "20kΩ" H 6881 4155 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3386C_Horizontal" H 6950 4200 50  0001 C CNN
F 3 "~" H 6950 4200 50  0001 C CNN
	1    6950 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT_TRIM RV2
U 1 1 5FC9C0D5
P 5850 4200
F 0 "RV2" H 5781 4246 50  0000 R CNN
F 1 "20kΩ" H 5781 4155 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3386C_Horizontal" H 5850 4200 50  0001 C CNN
F 3 "~" H 5850 4200 50  0001 C CNN
	1    5850 4200
	1    0    0    1   
$EndComp
Wire Wire Line
	4350 2950 4400 2950
Wire Wire Line
	4400 2950 4400 3050
Connection ~ 4400 3050
NoConn ~ 1700 3200
NoConn ~ 1700 3400
NoConn ~ 1700 4000
NoConn ~ 1100 3800
NoConn ~ 1100 3700
Wire Wire Line
	3050 4250 2950 4250
Connection ~ 2950 4250
Wire Wire Line
	2950 4250 2950 4150
Wire Wire Line
	3900 4150 3950 4150
Wire Wire Line
	3950 4150 3950 4250
Connection ~ 3950 4250
$Comp
L power:+5V #PWR0101
U 1 1 5FD88D32
P 4050 4250
F 0 "#PWR0101" H 4050 4100 50  0001 C CNN
F 1 "+5V" H 4065 4423 50  0000 C CNN
F 2 "" H 4050 4250 50  0001 C CNN
F 3 "" H 4050 4250 50  0001 C CNN
	1    4050 4250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0102
U 1 1 5FD8CAC7
P 5900 2750
F 0 "#PWR0102" H 5900 2600 50  0001 C CNN
F 1 "+5V" H 5915 2923 50  0000 C CNN
F 2 "" H 5900 2750 50  0001 C CNN
F 3 "" H 5900 2750 50  0001 C CNN
	1    5900 2750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5900 2750 6000 2750
$Comp
L power:+5V #PWR0103
U 1 1 5FD96F01
P 7800 4200
F 0 "#PWR0103" H 7800 4050 50  0001 C CNN
F 1 "+5V" H 7815 4373 50  0000 C CNN
F 2 "" H 7800 4200 50  0001 C CNN
F 3 "" H 7800 4200 50  0001 C CNN
	1    7800 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 4200 7800 4350
Wire Wire Line
	7800 4350 7550 4350
Connection ~ 7550 4350
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5FD9A6B8
P 2950 4500
F 0 "#FLG0101" H 2950 4575 50  0001 C CNN
F 1 "PWR_FLAG" H 2950 4673 50  0000 C CNN
F 2 "" H 2950 4500 50  0001 C CNN
F 3 "~" H 2950 4500 50  0001 C CNN
	1    2950 4500
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5FD9BAE5
P 7800 4350
F 0 "#FLG0102" H 7800 4425 50  0001 C CNN
F 1 "PWR_FLAG" H 7800 4523 50  0000 C CNN
F 2 "" H 7800 4350 50  0001 C CNN
F 3 "~" H 7800 4350 50  0001 C CNN
	1    7800 4350
	-1   0    0    1   
$EndComp
Connection ~ 7800 4350
NoConn ~ 3900 2450
NoConn ~ 3050 4350
$Comp
L Device:C C1
U 1 1 5FDA46AB
P 4800 4200
F 0 "C1" H 4915 4246 50  0000 L CNN
F 1 "100n" H 4915 4155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 4838 4050 50  0001 C CNN
F 3 "~" H 4800 4200 50  0001 C CNN
	1    4800 4200
	1    0    0    -1  
$EndComp
Connection ~ 4800 4050
Wire Wire Line
	4800 4050 4500 4050
Connection ~ 4800 4350
Wire Wire Line
	4800 4350 5850 4350
$Comp
L Device:R R10
U 1 1 5FDA8C13
P 7200 4350
F 0 "R10" V 6993 4350 50  0000 C CNN
F 1 "100R" V 7084 4350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 7130 4350 50  0001 C CNN
F 3 "~" H 7200 4350 50  0001 C CNN
	1    7200 4350
	0    1    1    0   
$EndComp
Wire Wire Line
	7050 4350 6950 4350
Wire Wire Line
	550  3200 550  2450
Wire Wire Line
	550  3200 800  3200
Wire Wire Line
	500  2650 500  3600
Wire Wire Line
	500  3600 800  3600
$EndSCHEMATC
